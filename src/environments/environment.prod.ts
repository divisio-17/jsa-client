export const environment = {
  environmentName: "production",
  production: true,
  skipAuthorizationChecks: false,
  api: '/api/'
};
